/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/**
 *
 * @author damt201
 */
public class Actividad1 {

    public static void main(String[] args) {
       
        File f = new File("pruebas.txt");
        escribirfichero(f, "el mensaje que quieres que escriba\nFIN");
        //escribirfichero(f, "FIN");
    }
    
    public static void escribirfichero(File fichero, String messaje){
        
        BufferedWriter b = null;
        try {
            b = new BufferedWriter(new FileWriter(fichero));
            b.write(messaje);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }finally{
            try {
                b.close();
            } catch (IOException ex) {
            }
        }
        
    }
    
}
