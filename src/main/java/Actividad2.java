
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

 /*
 Crea un método en java que lea un fichero de texto línea a línea
 */
/**
 *
 * @author damt201
 */
public class Actividad2 {

    public static void main(String[] args) throws IOException {

        File f = new File("prueba.txt");
        escribirfichero(f, "el mensaje que quieres que escriba\nFIN");
        leerFichero(f);
        File f2 = new File("prueba.props");
        //configurar(f2);
        Properties p = new Properties();
        leerPropiedades(p,f2);

    }

    public static void escribirfichero(File fichero, String messaje) {

        BufferedWriter b = null;
        try {
            b = new BufferedWriter(new FileWriter(fichero));
            b.write(messaje);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        } finally {
            try {
                b.close();
            } catch (IOException ex) {
            }
        }

    }

    public static void leerFichero(File fichero) throws IOException {

        try {
            FileReader f = new FileReader(fichero);
            BufferedReader br = new BufferedReader(f);

            String linea;
            while ((linea = br.readLine()) != null) {

                System.out.println(linea);

            }
            //br.close;

        } catch (FileNotFoundException e) {

        } catch (IOException ex) {

        }

    }

    public static void configurar(File fichero) throws IOException {

        try {
            Properties config = new Properties();
            config.setProperty("USER", "admin");
            config.setProperty("PASSW", "admin_123");
            config.setProperty("SERVER", "localhost");
            config.setProperty("PORT", "3306");
            config.store(new FileOutputStream(fichero), "");
        } catch (FileNotFoundException fn) {
            System.out.println(fn.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        leerFichero(fichero);
    }

    public static void leerPropiedades(Properties p, File fichero){

        try {
            p.load(new BufferedReader(new FileReader(fichero)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Actividad2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Actividad2.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(Map.Entry<Object,Object>propiedad:p.entrySet()){
            System.out.println(propiedad);
        }

    }

}
